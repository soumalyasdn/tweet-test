// const Twitter = require('twitter');
// import { ETwitterStreamEvent, TweetStream, TwitterApi, ETwitterApiError } from 'twitter-api-v2';
const { ETwitterStreamEvent, TwitterApi } = require('twitter-api-v2');
// const Twit = require('twit')
const config = require('config');
const SearchKey = require('../../models/searchKey.model');

module.exports = (app, io) => {

    // const twitter = new TwitterApi({
    //     appKey: config.get('TWITTER_CONSUMER_KEY'),
    //     appSecret: config.get('TWITTER_CONSUMER_SECRET'),
    //     // Following access tokens are not required if you are
    //     // at part 1 of user-auth process (ask for a request token)
    //     // or if you want a app-only client (see below)
    //     accessToken: config.get('TWITTER_ACCESS_TOKEN_KEY'),
    //     accessSecret: config.get('TWITTER_ACCESS_TOKEN_SECRET'),
    //   });

    const twitter = new TwitterApi(config.get('TWITTER_BEARER_TOKEN'));

    
    let socketConnection;
    let twitterStream;

    app.locals.searchTerm = ''; //Default search term for twitter stream.
    app.locals.showRetweets = false; //Default

    /**
     * Resumes twitter stream.
     */
    const stream = () => {
        if (app.locals.searchTerm) {
            console.log('Resuming for ' + app.locals.searchTerm);

            // twitter.v1.filterStream({
            // twitter.v2.searchStream({
            //     // See FilterStreamParams interface.
            //     keyword: 'JavaScript',
            //   }, (stream) => {
                

                // Not needed to await this!
                // const stream = twitter.v2.searchStream({URLSearchParams:'tweet.fields=author_id,created_at,&user.fields=created_at,name,profile_image_url,url,username&expansions=author_id', autoConnect: false });
                
                // const stream = twitter.v2.searchStream({params:'tweet.fields=author_id,created_at', autoConnect: false });
                const stream = twitter.v2.searchStream({autoConnect: false });
                // const stream = twitter.v1.stream.getStream('statuses/filter.json', { track: 'JavaScript,TypeScript' });

                stream.on(
                    // Emitted when Node.js {response} emits a 'error' event (contains its payload).
                    ETwitterStreamEvent.ConnectionError,
                    err => console.log('Connection error!', err),
                  );
                  
                  stream.on(
                    // Emitted when Node.js {response} is closed by remote or using .close().
                    ETwitterStreamEvent.ConnectionClosed,
                    () => console.log('Connection has been closed.'),
                  );

                // Assign yor event handlers
                // Emitted on Tweet
                // stream.on(ETwitterStreamEvent.Data, console.log);
                stream.on(ETwitterStreamEvent.Data, (tweet) => {
                    // console.log(tweet);
                    sendMessage(tweet);

                    
                });
                // Emitted only on initial connection success
                stream.on(ETwitterStreamEvent.Connected, () => console.log('Stream is started.'));

                // Start stream!
                stream.connect({autoReconnect: true, autoReconnectRetries: Infinity });
                
                
                twitterStream = stream;
            // });      

            // twitter.stream('statuses/filter', { track: app.locals.searchTerm }, (stream) => {
            //     console.log(stream);
                
            //     stream.on('data', (tweet) => {
            //         sendMessage(tweet);
            //     });

            //     stream.on('error', (error) => {
            //         console.log(error);
            //     });

            //     twitterStream = stream;
            // });
        }
    };

    /**
     * get first 25 tweets based on search item
     */
    /*app.post('/setSearchTerm', (req, res) => {
        //twitterStream.destroy();
        let term = req.body.searchTerm;
        app.locals.searchTerm = term;
        let params = {
            q: term,
            count: 25
        }
        twitter.get('search/tweets', params, (err, tweets, response) => {
            //console('logdata : ', tweet);
            if (err) {
                res.status(500);
            } else {
                //res.status(200);
                res.json(tweets);
                if (twitterStream) {
                    twitterStream.destroy();
                }
                stream();
            }
        });
    })*/

    /**
     * Sets search term for twitter stream.
     */
    app.post('/setSearchTerm', async (req, res) => {
        let term = req.body.searchTerm;
        app.locals.searchTerm = term;
        if (twitterStream) twitterStream.destroy();
        try {
            /**
             * @desc store searched word with date in DB 
             */

                 
            const rules = await twitter.v2.streamRules();
            // console.log(rules);
            // Log every rule ID

            if(rules.data){
                rules.data.map(rule => deleteTerms(twitter, rule.id));

            }

            
            // Add rules
            const addedRules = await twitter.v2.updateStreamRules({
                add: [
                { value: term}
                ],
            });

            // console.log(addedRules);


            let payload = {searchedTerm: term};
            const searched = await SearchKey.update(payload, payload, {upsert: true, setDefaultsOnInsert: true})  

            res.json(searched);         
            
            await stream();
        } catch(err) {
            console.log(err)
            res.status(500).send('Server Error');
        }
    });

    /**
     * set tweet count
     */
    app.post('/setCount', async (req, res) => {
        let term = req.body.payload.searchTerm;
        let payload = req.body.payload
        try {
            const result = await SearchKey.updateOne({searchedTerm: term}, payload, {upsert: true, setDefaultsOnInsert: true});
            const maxTweeted = await SearchKey.findOne().sort('-notificationCounter');
            res.json(maxTweeted);
        } catch (err) {
            console.log('err :', err)
        }

    })

    /**
     * Pauses the twitter stream.
     */
    app.post('/pause', (req, res) => {
        console.log('Pause');
        twitterStream.destroy();
    });

    /**
     * Resumes the twitter stream.
     */
    app.post('/resume', (req, res) => {
        console.log('Resume');
        stream();
    });

    

    io.on('connection', function(socket) {
        let data = socket.handshake.auth;
        console.log('client connected');
        socketConnection = socket;
        console.log(socket.id);
    
        socket.on('disconnect', () => {
            console.log('disconnected');
        })
    });
    
    /**
     * Emits data from stream.
     * @param {String} msg 
     */
    const sendMessage = (msg) => {
        // if (msg.text.includes('RT')) {
        //     return;
        // }

        console.log(socketConnection.id);
        socketConnection.emit("tweets", msg);
    }
};

async function deleteTerms(twitter, id){
    console.log(id);
    if(id){        
        // Delete rules
        const deleteRules = await twitter.v2.updateStreamRules({
            delete: {
                ids: [id],
            },
        });
        console.log(deleteRules);
    }
}


